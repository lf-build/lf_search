"use strict"

var Restify = require("restify");
var bunyan = require("bunyan");
var elasticsearch = require('elasticsearch');

var SearchService = require("./SearchService");
var TenantService = require("./TenantService");
var RequestContext = require("./RequestContext");
var RequestLogger = require("./RequestLogger");
var Token = require("./Token");
var Configuration = require("./Configuration");

class Startup {

    constructor() {

        this.services = {
            logger: function () {
                return bunyan.createLogger({
                    name: 'search',
                    service: 'search',
                    stream: process.stdout
                });
            },
            requestLogger: function () {
                if (this._logger === null) {
                    this._logger = new RequestLogger(this.logger());
                }
                return this._logger;
            },
            elasticsearch: function () {
                var client = new elasticsearch.Client({
                    host: process.env.ELASTICSEARCH_URL || "10.100.0.13:7098",
                    log: "trace"
                });
                return client;
            },
            configuration : function(){
                var url = process.env.CONFIGURATION_URL || "http://10.100.0.13:7001";
                return new Configuration(this.token(), url);
            },
            requestContext: function () {
                throw new Error("requestContext not available.");
            },
            tenant: function () {
                return new TenantService(this.token());
            },
            search: function () {
                return new SearchService(this.tenant(), this.elasticsearch());
            },
            token : function(){
                return new Token(this.requestContext());
            }
        }

    }

    createRoutes(server) {
        var self = this;
        var logger = self.services.logger();

        var logError = function (response, code, error) {
            logger.error(error);

            if (!response.finished) {
                response.send(code, error);
            }
        };

        server.get("/ping", function (request, response) {
            response.send(200, 'pong');
        });


        server.post("/:index/search", function (req, res) {
            try {

                self.services.search().search(req.params.index, req.body)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.post("/:index/:name/search", function (req, res) {
            try {
                self.services.configuration().get("saved-searches/" + req.params.name).then(function(query){
                    self.services.search().searchTemplate(req.params.index, query, req.body)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
                }).catch(function(){
                    return res.send(404);
                });
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.post("/:index/:name/count", function (req, res) {
            try {
                self.services.configuration().get("saved-searches/" + req.params.name).then(function(query){
                    self.services.search().countTemplate(req.params.index, query, req.body)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
                }).catch(function(){
                    return res.send(404);
                });
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.post("/:index/:key", function (req, res) {
            try {

                self.services.search().add(req.params.index,req.params.key, req.body)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.patch("/:index/:key", function (req, res) {
            try {

                self.services.search().update(req.params.index,req.params.key, req.body)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.patch("/:index/:key/tags", function (req, res) {
            try {
                self.services.search().addTags(req.params.index,req.params.key, req.body.tags)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.del("/:index/:key/tags", function (req, res) {
            try {
                self.services.search().removeTags(req.params.index,req.params.key, req.body.tags)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.del("/:index/:key", function (req, res) {
            try {

                self.services.search().delete(req.params.index,req.params.key)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });

        server.get("/:index/:key", function (req, res) {
            try {

                self.services.search().get(req.params.index,req.params.key)
                    .then(value => value != null ? res.send(200, value) : res.send(404))
                    .catch(error => logError(res, 400, error));
            } catch (error) {
                logError(res, 500, error);
            }
        });
    }

    setupCors(server) {
        Restify.CORS.ALLOW_HEADERS.push("Authorization", "authorization");
        server.use(Restify.CORS());
    }

    run() {

        var self = this;
        var log = self.services.requestLogger();
        var server = Restify.createServer();


        self.setupCors(server);

        server.use(Restify.acceptParser(server.acceptable));
        server.use(Restify.queryParser());
        server.use(Restify.bodyParser());
        server.use(Restify.gzipResponse());

        server.use(function (request, response, next) {
            self.services.requestContext = function () {
                return new RequestContext(request);
            };
            return next();
        });

        server.use(function (request, response, next) {
            response.on("finish", function () {
                log.info(request, response);
            });

            try {
                next();
            } catch (error) {
                log.error(request, response, error);
                return;
            }
        });


        self.createRoutes(server);

        server.listen(5000, function () {
            self.services.logger().info("server started");
        });
    }
}

new Startup().run();