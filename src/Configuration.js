"use strict";

var unirest = require("unirest");

class Configuration {
    constructor(token, url) {
        this._url = url;
        this._token = token;
    }
    get(service) {
        var self = this;
        return new Promise((resolve, reject) => {
            unirest.get(self._url + "/" + service)
                .header("Authorization", "Bearer " + this._token.read())
                .header("Content-Type", "application/json")
                .end(function (response) {
                    if (response.statusCode !== 200) {
                        reject(new Error(response.body.message));
                    }
                    else {
                        resolve(response.body);
                    }
                });
        });
    }
}

module.exports = Configuration;
