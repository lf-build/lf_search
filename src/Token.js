"use strict";

var jwt = require("jsonwebtoken");

class RequestToken {
    constructor(requestContext) {
        var rawSecretKey = process.env.SECRET_KEY;
        if (rawSecretKey === undefined) { throw new Error('SECRET_KEY environment variable not found'); }
        this._key = Buffer.from(rawSecretKey, 'base64');
        this._requestContext = requestContext;
    }

    read() {
        return this._requestContext
            .request
            .header("Authorization")
            .replace("Bearer", "")
            .trim();
    }

    decode() {
        var token = arguments.length === 1 ? arguments[0] : this.read();
        return jwt.verify(token, this._key, { ignoreExpiration: true });
    }

    isValid() {
        try {
            return this.decode() !== undefined;
        } catch (error) {
            return false;
        }
    }
}

module.exports = RequestToken;