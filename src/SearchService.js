"use strict";

class SearchService {

    constructor(tenantService, elasticSearch) {
        this.client = elasticSearch;
        this.tenantService = tenantService;
    }
    createKey(keys) {
        var lowerCaseKeys = [];
        lowerCaseKeys.push(this.tenantService.current().getId().toLowerCase());
        keys.forEach(key => {
            lowerCaseKeys.push(key.toLowerCase());
        });
        return lowerCaseKeys.join("-");
    }

    createIndexIfNotExist(index) {
        var self = this;
        return self.client.indices.exists({
            index: index
        }).then(function (indexExists) {
            if (!indexExists) {
                return self.client.indices.create({
                    index: index
                }).catch(function () {
                    return Promise.resolve(true);
                });
            } else {
                return Promise.resolve(true);
            }
        });
    }

    search(index, body) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.search({
                index: index,
                body: body
            }).then(function (response) {
                if (response.hits.total === 0) {
                    return null;
                }
                var queryResults = {
                    total: response.hits.total
                }
                for (var i = 0, hits = response.hits.hits; i < hits.length; i++) {
                    var data = hits[i]._source;
                    data.id = hits[i]._id;
                    data._score=hits[i]._score;
                    data._highlight=hits[i].highlight;
                    queryResults[hits[i]._type] = queryResults[hits[i]._type] || [];
                    queryResults[hits[i]._type].push(data);
                }
                return queryResults;
            });
        });
    }

    searchTemplate(index, source, params) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.searchTemplate({
                index: index,
                body: {
                    source: source,
                    params: params
                }
            }).then(function (response) {
                if (response.hits.total === 0) {
                    return null;
                }
                var queryResults = {
                    total: response.hits.total
                }
                for (var i = 0, hits = response.hits.hits; i < hits.length; i++) {
                    var data = hits[i]._source;
                    data.id = hits[i]._id;
                    data._score=hits[i]._score;
                    data._highlight=hits[i].highlight;
                    queryResults[hits[i]._type] = queryResults[hits[i]._type] || [];
                    queryResults[hits[i]._type].push(data);
                }
                return queryResults;
            });
        });
    }

    countTemplate(index, source, params) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.renderSearchTemplate({
                body: {
                    source: source,
                    params: params
                }
            }).then(function (response) {
                var template = response.template_output;
                delete template["from"];
                delete template["size"];
                return self.client.count({
                    index: index,
                    body: template
                }).then(function (response) {
                    return {
                        count: response.count
                    };
                });
            });
        });
    }

    add(index, key, object) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.index({
                index: index,
                type: "report",
                id: key,
                body: object
            });
        });
    }

    update(index, key, object) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.update({
                index: index,
                type: "report",
                id: key,
                body: {
                    doc: object,
                    doc_as_upsert: true
                }
            });
        });
    }

    delete(index, key) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.delete({
                index: index,
                type: "report",
                id: key
            });
        });
    }

    get(index, key) {
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.get({
                index: index,
                type: "report",
                id: key
            }).then(function (response) {
                if (response === undefined || response === null || response.found === false) {
                    return null;
                } else {
                    return response._source;
                }
            }).catch(function (response) {
                if (response.statusCode === 404) {
                    return null;
                } else {
                    throw (response);
                }
            });
        });
    }

    addTags(index, key, tags) {
        if (tags.length <= 0) {
            throw Error("At least one tag is required");
        }
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.update({
                index: index,
                type: "report",
                id: key,
                body: {
                    script: {
                        source: "if(ctx._source.tags==null){ctx._source.tags=new ArrayList();} ctx._source.tags.addAll(params.tags)",
                        lang: "painless",
                        params: {
                            "tags": tags
                        }
                    },
                    upsert: {
                        "tags": []
                    }
                }
            });
        });
    }

    removeTags(index, key, tags) {
        if (tags.length <= 0) {
            throw Error("At least one tag is required");
        }
        var self = this;
        index = self.createKey([index]);
        return self.createIndexIfNotExist(index).then(function () {
            return self.client.update({
                index: index,
                type: "report",
                id: key,
                body: {
                    script: {
                        source: "if(ctx._source.tags==null){ctx._source.tags=new ArrayList();} ctx._source.tags.removeAll(params.tags)",
                        lang: "painless",
                        params: {
                            "tags": tags
                        }
                    },
                    upsert: {
                        "tags": []
                    }
                }
            });
        });
    }

}

module.exports = SearchService;