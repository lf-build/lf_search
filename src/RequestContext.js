"use strict"

class RequestContext {

    constructor(request){
        this.request = request;
    }

    get request() {
        return this._request;
    }
    set request(value){
        this._request = value;
    }
}

module.exports = RequestContext;