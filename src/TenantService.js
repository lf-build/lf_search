"use strict"

var jwt = require("jsonwebtoken");

class TenantService {
    
    constructor(token){
        this.token = token;
    }

    current() {
        return new Tenant(this.token.decode().tenant);
    }
}

class Tenant {
    constructor(id){
        if(!id) throw new Error("Tenant ID is required");
        this.id = id;
    }

    getId() {
        return this.id;
    }
}

module.exports = TenantService;