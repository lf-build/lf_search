"use strict"

module.exports = class RequestLogger {

    constructor(logger) {
        this._logger = logger;
    }

    getProperty(obj, prop) {
        for (var key in obj) {
            if (key.toLowerCase() == prop.toLowerCase()) {
                return key;
            }
        }
    }
    removeProperties(key, obj) {
        var self = this;
        if (!key || !obj) return;
        if (typeof obj === 'object') {
            var propertyToRemove = self.getProperty(obj, key);
            if (propertyToRemove) {
                obj[propertyToRemove] = "XXXX";
            }
            Object.keys(obj).forEach(function (x) {
                self.removeProperties(key, obj[x]);
            });
        } else if (Array.isArray(obj)) {
            obj.forEach(function (val) {
                self.removeProperties(key, val);
            });
        }
    }

    cleanRequestBody(body) {
        var propertiesToExclude = process.env.LOG_EXCLUDEPROPERTIES;
        if (propertiesToExclude) {
            var properties = propertiesToExclude.split(',');
            properties.forEach(property => {
                this.removeProperties(property, body);
            });
        }
    }
    cleanHeaders(headers) {
        var headerCopy = {};
        if (headers) {
            for (var key in headers) {
                headerCopy[key] = headers[key];
                if (key.toLowerCase() == "authorization") {
                    var headerValue = headerCopy[key];
                    headerValue = headerValue.replace("Bearer", "").trim();
                    var splittedToken = headerValue.split('.');
                    var payload = splittedToken[1];
                    headerCopy[key] = JSON.parse(Buffer.from(payload, 'base64').toString());
                }

            }
        }
        return headerCopy;
    }
    info(request, response) {
        this.cleanRequestBody(request.body);
        this._logger.info({
            request: {
                id: request.getId(),
                method: request.method,
                url: request.url,
                query: (typeof request.query === 'function') ? request.query() : request.query,
                headers: this.cleanHeaders(request.headers),
                body: {
                    content: JSON.stringify(request.body || {})
                }
            },
            response: {
                status: response.statusCode,
                headers: response._headers
            }
        });
    }

    error(request, response, error) {
        this.cleanRequestBody(request.body);
        this._logger.error({
            request: {
                id: request.getId(),
                method: request.method,
                url: request.url,
                query: (typeof request.query === 'function') ? request.query() : request.query,
                headers: this.cleanHeaders(request.headers),
                body: {
                    content: JSON.stringify(request.body || {})
                }
            },
            response: {
                status: response.statusCode,
                headers: response._headers
            },
            error: error
        });
    }
}