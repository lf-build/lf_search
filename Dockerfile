FROM node:9-slim

WORKDIR /app
ADD /package.json /app/package.json
RUN npm install --only=production

RUN npm install -g pm2

ADD /src /app

#ENTRYPOINT node Startup.js
ENTRYPOINT pm2-docker --raw start Startup.js