var express = require('express');
var elasticSearch = require('elasticsearch');
var bodyParser = require('body-parser');
var endpoints = require("./app/endpoints.js")
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type');

    next();
}

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);
app.use(endpoints(express.Router()));

var port = process.env.PORT || 5000;
app.listen(port, function() {
    console.log("Running server at: ", port);
})

module.exports = app;